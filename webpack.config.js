const path                      = require('path');																		// Для корректных путей к файлу
const MiniCssExtractPlugin      = require('mini-css-extract-plugin');									// Для сборки CSS в отдельные файлы
const HtmlWebpackPlugin         = require('html-webpack-plugin');											// Для генерации HTML
const CopyWebpackPlugin         = require('copy-webpack-plugin');											// Для копирования файлов (шрифты/картинки)
const ImageminPlugin            = require('imagemin-webpack-plugin').default;					// Для оптимизации изображений
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");					// Чтобы из LESS/Sass/CSS в точках входа не делать JS

const isProduction = (process.env.npm_lifecycle_event === 'production');

// Параметры PostCSS для версии для разработки (без минификации)
const postCSSPLuginsDev  = [
	require('postcss-import')(),
	require('postcss-cssnext')({
		browsers : [
			'last 5 versions',
			'> 0.5%',
			'ie > 9'
		],
		cascade : false
	}),
]

// Параметры PostCSS для версии продакшн (сжатые css)
const postCSSPLuginsProd = [
	require('postcss-import')(),
	require('postcss-cssnext')({
		browsers : [
			'last 5 versions',
			'> 0.5%',
			'ie > 9'
		],
		cascade : false
	}),
	require('cssnano')({
		convertValues : {
			length : false
		},
		discardComments : {
			removeAll : true
		},
		discardUnused : false,
		zindex        : false
	}),
]

const config = {
	// base source path
	context : path.resolve(__dirname, 'src'),

	// entry file names to compile
	entry: {
		// JS
		script : path.resolve(__dirname, 'src/js/script.js'),
		// libsjs : path.resolve(__dirname, 'src/js/libsjs.js'),

		// Styles
		style : path.resolve(__dirname, 'src/styles/style.less'),
		libs  : path.resolve(__dirname, 'src/styles/libs.less'),
		fonts : path.resolve(__dirname, 'src/styles/fonts.less')
	},

	// compiled files path
	output: {
		filename   : "js/[name].min.js",
		path       : path.resolve(__dirname, 'dist'),
		publicPath : "/"
	},

	// directory for starting webpack dev server
	devServer: {
		overlay     : true,
		contentBase : path.resolve(__dirname, 'dist'),
		writeToDisk : true
	},

	// enable source maps
	devtool: isProduction ? '' : 'eval-sourcemap',

	module: {
		rules: [
			//  Scripts loader
			{
				test    : /\.js$/,
				exclude : [/node_modules/],
				loader  : 'babel-loader'
			},
			//  Fonts
			{
				test    : /\.(woff|woff2|eot|ttf|otf)/,
				loader  : 'file-loader',
				options : {
					name  : '[path][name].[ext]',
					limit : 8192,
					// publicPath: "../"
				},
			},
			{
				test : /\.pug$/,
				use  : [
					{
						loader  : 'pug-loader',
						options : {
							pretty : true
						}
					}
				]
			},
			// Style loader
			{
				test : /\.(c|le)ss$/,
				use  : [
					{
						loader  : MiniCssExtractPlugin.loader,
						options : {
							publicPath : "../"
						}
					},
					{
						loader  : 'css-loader',
						options : {
							importLoaders : 1,
							sourceMap     : true,
						}
					},
					{
						loader  :'postcss-loader',
						options : {
							sourceMap : true,
							plugins   : isProduction ? postCSSPLuginsProd : postCSSPLuginsDev
						}
					},
					{
						loader  :'less-loader',
						options : {
							importLoaders : 1,
							sourceMap     : true,
							relativeUrls  : true,
							strictMath    : true,
							noIeCompat    : true
						}
					},
				],
			},
			// Image loader (for styles)
			{
				test    : /\.(png|jpe?g|gif|ico|svg)$/,
				loaders : [
					{
						loader  : 'file-loader',
						options : {
							name  : '[path][name].[ext]',
							limit : 8192
						},
					},
					{
						loader  : 'img-loader',
						options : {
							plugins: [
								require('imagemin-gifsicle')({
									interlaced : false
								}),
								require('imagemin-mozjpeg')({
									progressive : true,
									arithmetic  : false
								}),
								require('imagemin-pngquant')({
									floyd : 0.5,
									speed : 2
								}),
								require('imagemin-svgo')({
									plugins: [
										{
											removeTitle           : true,
											convertPathData       : false,
											removeEmptyAttrs      : true,
											removeEmptyContainers : true
										}
									]
								})
							]
						}
					}
				]
			},
		]
	},
	plugins: [
		new FixStyleOnlyEntriesPlugin(),
		new MiniCssExtractPlugin({
			filename        : "./css/[name].min.css",
			useRelativePath : true,
		}),
		new CopyWebpackPlugin([
			{
				from : './img/',
				to   : './img/'
			},
			{
				from : './fonts/',
				to   : './fonts/'
			}
		]),
		new ImageminPlugin({
			test: /\.(jpe?g|png|gif|svg|ico)$/i,
			cacheFolder: path.resolve('./cache'),
		}),
		// // Desktop page
		new HtmlWebpackPlugin(
			{
				title    : 'Главная страница',
				filename : 'index.html',
				template : 'pages/index.pug',
				inject   : false
			}
		),
		// new HtmlWebpackPlugin(
		// 	{
		// 		title    : 'Дополнительная страница',
		// 		filename : 'page.html',
		// 		template : 'pages/page.pug',
		// 		inject   : false
		// 	}
		// ),
	]
};

module.exports = config;